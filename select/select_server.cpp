#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/select.h>

#define BUF_SIZE 512
#define MAX_CONN_COUNT 20

/**
 * 创建服务端socket，并返回socket文件描述符
 */
int createServerSocket(int listener_port) {
    // 创建服务端socket套接字
    int server_sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), client_sockfd = -1;
    // 将套接字和IP、端口绑定
    struct sockaddr_in server_addr;
    // 申请内存
    memset(&server_addr, 0, sizeof(server_addr)); 
    // IPv4地址
    server_addr.sin_family = AF_INET;  
    // IP地址
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    // 端口
    server_addr.sin_port = htons(listener_port); 
    // 绑定socket
    bind(server_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

    // 监听端口，最大连接数为20
    listen(server_sockfd, MAX_CONN_COUNT);

    printf("[debug] listening on %s:%d ...\n", "127.0.0.1", listener_port);
    return server_sockfd;
}

/**
 * 处理connect事件，添加read事件
 */
void handleConnectSocketData(int server_sockfd, fd_set *all_set, int *client_sockfds) {
    // 监听对应端口是否有事件触发
    printf("[debug] server_sockfd %d in read_fd_set...\n", server_sockfd);
    int fd;
    // 有事件触发 accept一个与客户端对应的socket
    struct sockaddr_in client_addr;
    socklen_t clnt_addr_size = sizeof(client_addr);
    int client_sockfd = accept(server_sockfd, (struct sockaddr*)&client_addr, &clnt_addr_size);
    if (client_sockfd > 0) {
        printf("<<<< client connected from %s:%d\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
        for (fd = 0; fd < FD_SETSIZE; fd++) {
            if (client_sockfds[fd] < 0) {
                client_sockfds[fd] = client_sockfd;
                break;
            } 
        }
        // 客户端socket加入fd_set
        FD_SET(client_sockfd, all_set);
    } else {
        printf("[error] accept error\n");
    }
}

/**
 * 处理read事件，添加write事件
 */
void handleReadSocketData(int client_sockfd, fd_set *all_set, int *client_sockfds) {
    printf("[debug] client_sockfd %d in read_fd_set...\n", client_sockfd);
    // 定义数据缓冲
    char buf[BUF_SIZE];
    ssize_t nread;
    // 清空数组，用于接收新消息
    memset(buf, 0, sizeof(buf)); 
    nread = recv(client_sockfd, buf, BUF_SIZE,  MSG_DONTWAIT);
    // Ignore failed request
    if (nread == -1) {
        printf("[error] recv error\n");
        exit(EXIT_FAILURE);                        
    } else if (nread == 0) {
        printf("[debug] remove client_sockfd in read_fd_set...\n");
        // 关闭socket，移除fd_set
        FD_CLR(client_sockfd, all_set); 
        close(client_sockfd); 
    } else {
        int received_size = (int)(strlen(buf));
        printf("<<<< Received request：%s, size: %d\n", buf, received_size);
        // 向客户端发送数据
        char feedback[] = "WriteResult({ \"nRemoved\" : 1 })";
        printf(">>>> Sending response: %s\n", feedback);
        write(client_sockfd, feedback, sizeof(feedback));
    }
}

int main() {
    // 定义服务端和客户端socket文件描述符
    int server_sockfd = -1, client_sockfd = -1;

    // 定义select相关
    int port = 6000, fd;
    int client_sockfds[FD_SETSIZE];
    for (fd = 0; fd < FD_SETSIZE; fd++)
        client_sockfds[fd] = -1;

    // 创建socket
    server_sockfd = createServerSocket(6000);
    // 定义句柄集合
    fd_set read_fd_set;
    fd_set all_set;
    // 清理句柄集合
    FD_ZERO(&read_fd_set);
    FD_ZERO(&all_set);
    // 将监听socket放入集合
    FD_SET(server_sockfd, &all_set);

    // 使用while来监听连接到socket的事件
    while (1) {
        read_fd_set = all_set;
        printf("[debug] start select...\n");
        // 使用select模型进行管理
        int retval  = select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL);
        printf("[debug] select value is: %d\n", retval);
        if (retval < 0) {
            printf("[debug] select error!!!\n");
        } else if (retval == 0){
            printf("[debug] select timeout!!!\n");
            continue;
        } else {
            if (FD_ISSET(server_sockfd, &read_fd_set)) {
                handleConnectSocketData(server_sockfd, &all_set, client_sockfds);
            }
            for (fd = 0; fd < FD_SETSIZE; fd++)  {
                client_sockfd = client_sockfds[fd];
                if (client_sockfd == -1) {
                    continue;
                }
                if (FD_ISSET(client_sockfd, &read_fd_set)) {
                   handleReadSocketData(client_sockfd, &all_set, client_sockfds);
                }
            }
        }
    }
    return 0;
}