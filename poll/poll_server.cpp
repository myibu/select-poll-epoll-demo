#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <poll.h>

#define BUF_SIZE 512
#define CLIENT_SIZE 20
#define MAX_CONN_COUNT 20

/**
 * 创建服务端socket，并返回socket文件描述符
 */
int createServerSocket(int listener_port) {
    // 创建服务端socket套接字
    int server_sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), client_sockfd = -1;
    // 将套接字和IP、端口绑定
    struct sockaddr_in server_addr;
    // 申请内存
    memset(&server_addr, 0, sizeof(server_addr)); 
    // IPv4地址
    server_addr.sin_family = AF_INET;  
    // IP地址
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    // 端口
    server_addr.sin_port = htons(listener_port); 
    // 绑定socket
    bind(server_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));

    // 监听端口，最大连接数为20
    listen(server_sockfd, MAX_CONN_COUNT);

    printf("[debug] listening on %s:%d ...\n", "127.0.0.1", listener_port);
    return server_sockfd;
}

/**
 * 处理connect事件，添加read事件
 */
void handleConnectSocketData(int server_sockfd, int *connected_count, struct pollfd *sockfds) {
    // 监听对应端口是否有事件触发
    printf("[debug] new client connecting...\n");
    // 有事件触发 accept一个与客户端对应的socket
    struct sockaddr_in client_addr;
    socklen_t clnt_addr_size = sizeof(client_addr);
    int client_sockfd = accept(server_sockfd, (struct sockaddr*)&client_addr, &clnt_addr_size);
    if (client_sockfd > 0) {
        printf("<<<< client connected from %s:%d\n",inet_ntoa(client_addr.sin_addr),ntohs(client_addr.sin_port));
        *connected_count = *connected_count+1;
        sockfds[*connected_count].fd = client_sockfd;
        sockfds[*connected_count].events = POLLIN | POLLERR;
        sockfds[*connected_count].revents = 0;
    } else {
        printf("[error] accept error\n");
        exit(EXIT_FAILURE);
    }
}

/**
 * 处理read事件，添加write事件
 */
void handleReadSocketData(int fd, int *connected_count, struct pollfd *sockfds) {
    printf("[debug] client %d is readable...\n", sockfds[fd].fd);
    // 定义数据缓冲
    char buf[BUF_SIZE];
    ssize_t nread;
    int n;
    // 清空数组，用于接收新消息
    memset(buf, 0, sizeof(buf)); 
    nread = recv(sockfds[fd].fd, buf, BUF_SIZE,  MSG_DONTWAIT);
    // Ignore failed request
    if (nread == -1) {
        printf("[error] recv error\n");
        exit(EXIT_FAILURE);                
    } else if (nread == 0) {
        // 关闭socket
        printf("[debug] client exit...\n");
        close(sockfds[fd].fd); 
        // 移除该fd
        for (n = fd; n < *connected_count+1; n++) {
            if (n+1 < *connected_count+1) {
                sockfds[n] = sockfds[n+1];
            }
        }
        *connected_count = *connected_count -1;
    } else {
        int received_size = (int)(strlen(buf));
        printf("<<<< Received request：%s, size: %d\n", buf, received_size);
        sockfds[fd].events &= (~POLLIN);
        sockfds[fd].events |= POLLOUT;
    }
}

/**
 * 处理write事件，添加read事件
 */
void handleWriteSocketData(int fd, int *connected_count, struct pollfd *sockfds) {
    printf("[debug] client %d is writeable...\n", sockfds[fd].fd);
    // 向客户端发送数据
    char feedback[] = "WriteResult({ \"nRemoved\" : 1 })";
    printf(">>>> Sending response: %s\n", feedback);
    write(sockfds[fd].fd, feedback, sizeof(feedback));
    sockfds[fd].events &= (~POLLOUT);
    sockfds[fd].events |= POLLIN;
}

int main() {
    // 定义服务端和客户端socket文件描述符
    int server_sockfd = -1, client_sockfd = -1;
    // 定义poll相关
    int port = 6000, fd, n, connected_count = 0;
    struct pollfd sockfds[CLIENT_SIZE];
    memset(sockfds, 0, sizeof(sockfds)); 

    // 创建socket
    server_sockfd = createServerSocket(6000);
    // 初始化poll
    sockfds[0].fd = server_sockfd;
    sockfds[0].events = POLLIN | POLLERR;
    sockfds[0].revents = 0;
    
    // 使用while来监听连接到socket的事件
    while (1) {
        printf("[debug] start poll...\n");
        // 使用poll模型进行管理
        int retval = poll(sockfds, connected_count+1, -1);
        printf("[debug] poll value is: %d\n", retval);
        if (retval < 0) {
            printf("[error] poll error!!!\n");
            exit(EXIT_FAILURE);
        } 
        for (fd = 0; fd < connected_count+1; fd++)  {
            // 客户端返回错误
            if ((sockfds[fd].revents & POLLRDHUP) || (sockfds[fd].revents & POLLERR)) {
                printf("[error] poll error\n");
                exit(EXIT_FAILURE);
            }
            // 端口有新的连接
            else if ((sockfds[fd].fd == server_sockfd) && (sockfds[fd].revents & POLLIN)) {
                handleConnectSocketData(server_sockfd, &connected_count, sockfds);
            } 
            // 可读
            else if (sockfds[fd].revents & POLLIN){
                handleReadSocketData(fd, &connected_count, sockfds);
            }
            // 可写
            else if (sockfds[fd].revents & POLLOUT){
               handleWriteSocketData(fd, &connected_count, sockfds);
            }
        }
    }
    return 0;
}