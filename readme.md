# Select-Poll-Epoll-Demo
Demo of server-client application using select、poll、epoll and socket.

## Implements
### [select.h](https://www.man7.org/linux/man-pages/man2/select.2.html)
```CPP
#include <sys/select.h>

int select(int nfds, fd_set *restrict readfds,
            fd_set *restrict writefds, fd_set *restrict exceptfds,
            struct timeval *restrict timeout);

void FD_CLR(int fd, fd_set *set);
int  FD_ISSET(int fd, fd_set *set);
void FD_SET(int fd, fd_set *set);
void FD_ZERO(fd_set *set);

int pselect(int nfds, fd_set *restrict readfds,
            fd_set *restrict writefds, fd_set *restrict exceptfds,
            const struct timespec *restrict timeout,
            const sigset_t *restrict sigmask);
```

### [poll.h](https://www.man7.org/linux/man-pages/man2/poll.2.html)
```CPP
#include <poll.h>

int poll(struct pollfd *fds, nfds_t nfds, int timeout);

#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <poll.h>

int ppoll(struct pollfd *fds, nfds_t nfds,
            const struct timespec *tmo_p, const sigset_t *sigmask);
```

### [epoll.h](https://www.man7.org/linux/man-pages/man7/epoll.7.html)
```CPP
#include <sys/epoll.h>

int epoll_create(int size);
int epoll_create1(int flags);

int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);

int epoll_wait(int epfd, struct epoll_event *events,
            int maxevents, int timeout);
int epoll_pwait(int epfd, struct epoll_event *events,
            int maxevents, int timeout,
            const sigset_t *sigmask);
int epoll_pwait2(int epfd, struct epoll_event *events,
            int maxevents, const struct timespec *timeout,
                      const sigset_t *sigmask);
```
### [socket.h](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/sys_socket.h.html)
```CPP
int     accept(int, struct sockaddr *restrict, socklen_t *restrict);
int     bind(int, const struct sockaddr *, socklen_t);
int     connect(int, const struct sockaddr *, socklen_t);
int     getpeername(int, struct sockaddr *restrict, socklen_t *restrict);
int     getsockname(int, struct sockaddr *restrict, socklen_t *restrict);
int     getsockopt(int, int, int, void *restrict, socklen_t *restrict);
int     listen(int, int);
ssize_t recv(int, void *, size_t, int);
ssize_t recvfrom(int, void *restrict, size_t, int,
        struct sockaddr *restrict, socklen_t *restrict);
ssize_t recvmsg(int, struct msghdr *, int);
ssize_t send(int, const void *, size_t, int);
ssize_t sendmsg(int, const struct msghdr *, int);
ssize_t sendto(int, const void *, size_t, int, const struct sockaddr *,
        socklen_t);
int     setsockopt(int, int, int, const void *, socklen_t);
int     shutdown(int, int);
int     sockatmark(int);
int     socket(int, int, int);
int     socketpair(int, int, int, int [2]);
```

## Usage
- Run one server(like select_server): 
```
$ cd select-poll-epoll-demo/select
$ chmod +x server_start.sh
$ ./server_start.sh
[debug] listening on 127.0.0.1:6000 ...
[debug] start select...
```

- Run one or more client(like telnet):
```
$ telnet 127.0.0.1 6000
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
```

- Quit telnet client(press 'ctrl' and ']' in the same time on keyboard, then input 'quit' to telnet client):
```
# telnet 127.0.0.1 6000
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.
db.user.remove();
WriteResult({ "nRemoved" : 1 })^]

telnet> quit
Connection closed.
```